#include <Wire.h>
#include <EEPROM.h>
#include <LiquidCrystal_I2C.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define DEBUGMODE

#ifdef DEBUGMODE
    #define Sprint(b) (Serial.print(b))
    #define Sprintln(b) (Serial.println(b))
#else  
    #define Sprint(b)
    #define Sprintln(b)
#endif


enum enumButtons
{
  btSelect=0,
  btDown,
  btUp,
  btEnter,
};

enum enumLevel
{
	levelRootMenu=0,
	levelOneMenu,
	levelRefreshlcd,
};
//////////
const byte  totalButtons= 4, // Πληθος μπουτον
columnLCD= 16, //διαστασεις οθονης
lineLCD= 2,  //διαστασεις οθονης
pinMosfet= 11,
ONE_WIRE_BUS= 6;

const byte buttonsPinArray[totalButtons]={10,3,2,8};

const bool
powerON = 1,
powerOFF = 0,
UP = 1,
DOWN =0,
FEED = 1,
WAIT = 0; 

byte
saveCheckByte = 0,
helpByteAfnMind = 9,
levelMenu = levelRefreshlcd; 


int
minWaitTimeROM = 0,
feedTimeROM = 1,
frequancyTimeROM = 0,
stepDownValueROM = 0,
menuSel =-1,
UpDown = 0,
countForExitMenu = 0,
countWrRomManualTempChange = 0,
helpTimerWait = 0,
helpTimerFeed = 0,
timeFeed = 1,
timeWaiting = 0;

float
nowTemp = 0,
setTempROM = 0,
oldTemp = 0;

bool
flagGeneral = 0,
flagEnterExitMenu = 0,
flagFeed = 0,
flagEnRefreshLcd = 0,
flagAfterRestartROM = 0,
flagFirstWrEepromROM = 0,
isTempUpOrDown = 0,
isLcdPrSymbolForViewMode = 0,
isReadOnceA = 0;

//mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
const byte BUf_EEPROM = 4;
////
enum eepromLocations
{
  eeFeedTime=0,
  eeMinWaitTime,
  eeFrequancyTime,
  eeStepValue,
  eeSetTempRom,
  eeAfterRestart,
  eeFirstWrEeprom,
};

int addressEeprom[] =   
{ 
  eeFeedTime * BUf_EEPROM,
  eeMinWaitTime * BUf_EEPROM,
  eeFrequancyTime * BUf_EEPROM,
  eeStepValue * BUf_EEPROM,
  eeSetTempRom * BUf_EEPROM,
  eeAfterRestart * BUf_EEPROM,
  eeFirstWrEeprom * BUf_EEPROM,
};
//mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
//δομη μενου
char* menuArr[] =    {
                       "1.feed time",
                       "2.min wait t",
                       "3.step down",
                       "4.frequancy",
                       "5.after rest",
                       "6.res defaults",
                       "7.exit menu",
                      };
//δομη υπομενου αντιστοιχια σε καθε μενου. ιδιοι σε μεγεθος!!!
char* subMenuArr[] = {
                        "1.1 sec: ",
                        "2.1 sec: ",
                        "3.1 sec: ",
                        "4.0 sec: ",
                        "5.1 on/off",
                        "6.1 exit/reset",
                        "7.0 ",
                       };

int sizeMenuArr = sizeof(menuArr) / sizeof(char*); //μεγεθος του πινακα μενου

OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature DS18B20(&oneWire);
LiquidCrystal_I2C lcd(0x3F,columnLCD,lineLCD);

//-----------------------------------------------------------------------------------
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void setup()
{
  flagFirstWrEepromROM = fn_eeRead(eeFirstWrEeprom,flagFirstWrEepromROM);
  if(!flagFirstWrEepromROM) {fn_firstWrEeprom();}
  // fn_firstWrEeprom();
 
  for(int i=0; i<totalButtons; i++)
    {
      pinMode(buttonsPinArray[i],  INPUT);
    }
  
  pinMode(pinMosfet, OUTPUT);
  digitalWrite(pinMosfet, 0); //off

  Serial.begin(9600);
  delay(100);
  DS18B20.begin();
  delay(100);
  lcd.init();
  lcd.backlight();
  delay(1000);
  fn_readEepromVal();
  // setTempROM = flagAfterRestartROM ==1 ? setTempROM : 15.9; 
  setTempROM = flagAfterRestartROM == 1 ? setTempROM : 16.8;
  levelMenu = levelRefreshlcd;
  fn_indexScreenLcd();
  fn_lcdOnOffMode();
  fn_mind();
}
////////////////////////////////////////////////////////
void loop()
{
	fn_readButtons();
	setTempROM > 17 ? fn_timers() : fn_timersOff();   
}
////////////////////////////////////////////////////////
void fn_mind()
{
  static bool localFlagBB = 0, localFlagCC = 0;

  float diff = setTempROM - nowTemp;
  Sprintln("diff");
  Sprintln(diff);
  Sprintln(helpByteAfnMind);

  if(diff > 3)
  {
   if(helpByteAfnMind!=1)
      {
        helpByteAfnMind=1;
        timeWaiting = minWaitTimeROM;     
      }
  }

  else if(diff < 3 && diff > 1)
  {
      if(helpByteAfnMind !=2)
      {
        helpByteAfnMind = 2;
        timeWaiting = 5 * minWaitTimeROM;    
      }
      if(oldTemp >= nowTemp)
      {
        timeWaiting -= (3*stepDownValueROM);
        isTempUpOrDown = DOWN;    
      }
      else
      {
        isTempUpOrDown = UP;
      }
  }

  else if(diff < 1 && diff > 0)
  {
      if(helpByteAfnMind != 3)
      {
        helpByteAfnMind = 3;
        localFlagBB == 0 ? timeWaiting = 7 * minWaitTimeROM : timeWaiting = timeWaiting-100;
        localFlagBB = 0;  
      }
      if(oldTemp < nowTemp)
      {
        // timeWaiting += stepDownValueROM;
        isTempUpOrDown = UP;    
      }
      else
      {
        timeWaiting -= (3 * stepDownValueROM);
        isTempUpOrDown = DOWN;
      }
  }

  else if(diff < 0)
  {
      if(helpByteAfnMind !=4)
      {
        helpByteAfnMind = 4;
        localFlagBB = 1;
        timeWaiting = 9 * minWaitTimeROM;   
      }
      
      if(oldTemp < nowTemp)
      {  
        isTempUpOrDown = UP;   
        timeWaiting += 50;
        fn_printToLcd("!",14,1);
      }
      else
      {
        timeWaiting -= (2 * stepDownValueROM);
        isTempUpOrDown = DOWN;
        // localFlagCC = 1;
      }
  }
    oldTemp = nowTemp;
    isLcdPrSymbolForViewMode = !isLcdPrSymbolForViewMode;
    timeWaiting = constrain(timeWaiting, minWaitTimeROM, minWaitTimeROM*10);
}
//////////////////////////////////////////
void fn_mainFeed()
{
  if(flagEnterExitMenu)
  return;

    flagFeed == 1 ? helpTimerFeed++ : helpTimerWait++;
    
    if(helpTimerWait+2 >= timeWaiting && !flagFeed)
    {
      helpTimerWait=0;
      fn_feedEffect();
      fn_feedOrWaiting(FEED);
      flagFeed=1;
    }
    else if(helpTimerFeed >= feedTimeROM && flagFeed)
    {
      flagFeed=0;
      helpTimerFeed=0;    
      fn_feedOrWaiting(WAIT);
    }
}
////////////////////
void fn_indexScreenLcd()
{
  flagEnRefreshLcd=1;
  lcd.clear();
  fn_printToLcd("NollaTem",4,0);
    for(byte i=0; i<15; i++)
    {
      fn_printToLcd("\xFF",i,1);
      delay(200);    
    }
  lcd.clear();
  flagEnRefreshLcd=0;
    // menuSel=0;
}
//////////////////////////////
///////////////////////////
void fn_menu()
{
    static bool localFlagPotiotionMenu=0;
    static byte helpByte=0;

  if(menuSel == sizeMenuArr)
  {
    menuSel=0;
    helpByte=0;    
  }
      if(menuSel % lineLCD == 0)
        {
          lcd.clear();
          fn_printToLcd("\x7F",0,columnLCD-1,0);
          localFlagPotiotionMenu=1;
          helpByte = menuSel;
        }
      else
        {
          for(byte ia=0; ia<lineLCD; ia++)
          {
            fn_printToLcd(" ",0,columnLCD-1,ia);    
          }
          fn_printToLcd("\x7F",0,columnLCD-1,menuSel - helpByte);
          localFlagPotiotionMenu=0;
        }

  if(levelMenu == levelRootMenu && localFlagPotiotionMenu)
  {
    for(byte i=0; i<lineLCD; i++)
    {   
      fn_printToLcd(menuArr[menuSel+i],0,i);
      Sprintln("menuSel");
      Sprintln(menuSel);
      Sprintln("i");
      Sprintln(i);
    }
  }

  if(levelMenu == levelOneMenu)   
  {
    switch (menuSel+1) 
    {
        case 1:
         fn_subMenu(feedTimeROM,eeFeedTime,1);
          break;

        case 2:
          fn_subMenu(minWaitTimeROM,eeMinWaitTime,2);
          break;

        case 3:
          fn_subMenu(stepDownValueROM,eeStepValue,1);
          break;

        case 4:
          fn_subMenu(frequancyTimeROM,eeFrequancyTime,1);
          break;

        case 5:
          // fn_afterRestart();
          fn_subMenuWithFlag("5.1  -> ON      ","5.1  -> OFF     ",eeAfterRestart);
          break;

        case 6:
        	fn_subMenuWithFlag("6.1  -> EXIT    ","6.1  -> RESET   ",eeFirstWrEeprom);
          break;

        case 7: //test buzz
          fn_exitMenu();
          break;
    }
  }
      Sprintln("menuSel ");
      Sprint(menuSel);
}
////////////
void fn_saveEffect()
{
  flagEnRefreshLcd=1;
  int t=700;
  lcd.clear();
  // fn_printToLcd(menuArr[menuSel],0,0,0);

  for(byte i=0; i<16; i++)
  {
      fn_printToLcd("\xFF",0,i,1);
      delay(100);
  }
  lcd.clear();
  fn_printToLcd("--- Save  Ok ---",0,0,0);
  delay(500);
  lcd.clear();
  lcd.display();

  levelMenu=levelRootMenu;
  menuSel=0;
  flagEnRefreshLcd=0;
  delay(100);

  for(byte i=0; i<lineLCD; i++)
    {   
      fn_printToLcd(menuArr[menuSel+i],0,i);
    }
}
////////////
void fn_readButtons()
{
  static byte i=0;
  static byte stateButtons[4]={0,0,0,0};
  static byte oldStateButtons[4]={0,0,0,0};
  static long startTimerHoldBt = 0;

  for(i=0; i<totalButtons; i++)
  {
    stateButtons[i] = digitalRead(buttonsPinArray[i]);

	  if(stateButtons[btSelect] && stateButtons[btEnter]) //enter menu
	  {
	    flagEnterExitMenu=1;
      levelMenu = levelRootMenu;
	    Sprintln("flagEnterExitMenu");
	    Sprintln(flagEnterExitMenu);
	    delay(200);
	    fn_menu();
	  }

    // if(!flagEnterExitMenu)
    // return;

    if(stateButtons[i] && !oldStateButtons[i])
    {
      if(levelMenu == levelRootMenu)
      {
        switch (i) 
            {
              case btSelect:
                menuSel++;
                 break;

              case btEnter:
                levelMenu=levelOneMenu;
                break;
            }
            fn_menu();
      	    countForExitMenu=45;    
      }
      else if(levelMenu==levelOneMenu)
      {
          switch (i) 
          {
                case btSelect:
                  levelMenu=levelRootMenu;
                  saveCheckByte = 0;
                  isReadOnceA = 0;
                  UpDown=0;
                  menuSel=0;
                   break;

                case btUp:
                  UpDown ++;
                  flagGeneral=1;
                   break;

                case btDown:
                  UpDown --;
                  flagGeneral=0;
                   break;

                case btEnter:
                  saveCheckByte++;
                   break;
          }
          countForExitMenu=45;
          fn_menu();
      }

      if(levelMenu == levelRefreshlcd)
      {
      	   switch (i) 
          {
	        case btSelect:
	           break;

	        case btUp:
	           setTempROM +=0.2;
             fn_lcdRefresh();
	           countWrRomManualTempChange=15;
	           break;

	        case btDown:
	          setTempROM -=0.2;
            fn_lcdRefresh();
	          countWrRomManualTempChange=15;
	           break;

	        case btEnter:
	        while(stateButtons[btEnter])
          {
             fn_feedOrWaiting(FEED);
             stateButtons[btEnter] = digitalRead(buttonsPinArray[btEnter]);
          }
             fn_feedOrWaiting(WAIT);
	           break;
           }
      }
    }
    delay(5);
    oldStateButtons[i] = stateButtons[i];

    //================press n hold bt up + down ==================================================
    // if(levelMenu == levelRefreshlcd && stateButtons[btSelect] && oldStateButtons[btSelect] && millis() - startTimerHoldBt > 1500)
    // {
    // 	while(stateButtons[btSelect])
    // 	{
    // 	  fn_feedOrWaiting(FEED);
    // 	  stateButtons[btSelect] = digitalRead(buttonsPinArray[btSelect]);
    // 	}
    // 	fn_feedOrWaiting(WAIT);
    // }

    // if(levelMenu == levelRefreshlcd && stateButtons[btDown] && oldStateButtons[btDown] && millis() - startTimerHoldBt > 1500)
    // {
    // 	lcd.clear();
    // 	delay(3000);
    // 	fn_eeWrite(eeSetTempRom,16.8f);
    // }
    //============================================================================================
    }
}
////////////
void fn_timers()
{
  static unsigned long loopTimerA=0;
  static byte byteHelpCounterA=0, byteHelpCounterB=0;
  int t=700;

  if(millis() - loopTimerA > 999)
    {
      fn_mainFeed();
      fn_lcdRefresh();
      if(countForExitMenu > -1) {countForExitMenu--;}
      if(countWrRomManualTempChange > -1) {countWrRomManualTempChange--;}
      byteHelpCounterA++;
      byteHelpCounterB++;
      loopTimerA = millis();
    }

    if(byteHelpCounterA >= frequancyTimeROM)
    {
      byteHelpCounterA=0;
      fn_mind();    
    }

    if(byteHelpCounterB > 9)
    {
    	byteHelpCounterB=0;     
    	fn_readDS18B20();
    }

    if(countWrRomManualTempChange == 0)
    {
      fn_eeWrite(eeSetTempRom,setTempROM);
      fn_printToLcd("*****",0,1);       
    }

    // exit menu procces
    if(countForExitMenu == 0)
      {
      lcd.clear();
      fn_printToLcd("-- Exit Menu --",0,0);
	    delay(2000);
	    fn_exitMenu();    
      }
} /////////////
void fn_timersOff()
{
 	static unsigned long localLoopTimerA=0;
  static byte localCounterA=0;

	if(millis() - localLoopTimerA > 999)
	{ 	    
    localCounterA++;
    if(countWrRomManualTempChange > -1) {countWrRomManualTempChange--;}
    localLoopTimerA = millis();
	}

  if(localCounterA > 9)
  {
      
    fn_readDS18B20();
    lcd.clear();    
    fn_lcdOnOffMode();
    localCounterA = 0;
  }

  if(countWrRomManualTempChange == 0)
  {
    fn_eeWrite(eeSetTempRom,setTempROM);       
  }
}
/////////
void fn_subMenu(int val, int locMem, byte step)
{
  if(!isReadOnceA)
  {
    isReadOnceA=1;
    UpDown = val;
  }
  
  lcd.clear();
    fn_printToLcd(menuArr[menuSel],0,0,0);
    fn_printToLcd(subMenuArr[menuSel],UpDown,0,1);

  
    if(saveCheckByte>1) //double btEnter for save
    {
      fn_eeWrite(locMem,UpDown);

      saveCheckByte=0;
      val = fn_eeRead(locMem,val);
      isReadOnceA=0;
      fn_readEepromVal();
      fn_saveEffect();
    }
}
//////////////////////////////
void fn_readEepromVal()
{
  frequancyTimeROM = fn_eeRead(eeFrequancyTime,frequancyTimeROM);
  Sprint("frequancyTimeROM ");
  Sprintln(frequancyTimeROM);

  minWaitTimeROM = fn_eeRead(eeMinWaitTime,minWaitTimeROM);
  Sprint("minWaitTimeROM ");
  Sprintln(minWaitTimeROM);

  stepDownValueROM = fn_eeRead(eeStepValue,stepDownValueROM);
  Sprint("stepDownValueROM ");
  Sprintln(stepDownValueROM);

  setTempROM = fn_eeRead(eeSetTempRom,setTempROM);
  Sprint("setTempROM ");
  Sprintln(setTempROM);

  flagAfterRestartROM = fn_eeRead(eeAfterRestart,flagAfterRestartROM);
  Sprint("flagAfterRestartROM ");
  Sprintln(flagAfterRestartROM);
  
  flagFirstWrEepromROM = fn_eeRead(eeFirstWrEeprom,flagFirstWrEepromROM);
  Sprint("flagFirstWrEepromROM ");
  Sprintln(flagFirstWrEepromROM);

}
////////////////////////////////////////////////////////
void fn_firstWrEeprom()
{
	fn_eeWrite(eeFrequancyTime,30);
	fn_eeWrite(eeMinWaitTime,45);
	fn_eeWrite(eeFeedTime,1);
	fn_eeWrite(eeStepValue,3);
	fn_eeWrite(eeAfterRestart,true);
	fn_eeWrite(eeFirstWrEeprom,true);
	fn_eeWrite(eeSetTempRom,16.8f);

	Sprintln("first write eeprom");
}
////////////////////////////////////////////////////////
//=================================================================================================================
void fn_eeWrite(int address, float value) //float
{
  EEPROM.put(addressEeprom[address], value);
  delay(100);
  // EEPROM.commit();
  // delay(100);
}
// /////////////////////////////
void fn_eeWrite(int address, bool value) //bool
{
  EEPROM.write(addressEeprom[address], value);
  delay(100);
  // EEPROM.commit();
  // delay(100);
}
////////////////////
void fn_eeWrite(int address, int value) //int
{
  EEPROM.put(addressEeprom[address], value);
  delay(100);
  // EEPROM.commit();
  // delay(100);
}
/////////////////////
float fn_eeRead(int address, float valType) //float
{
  float dataFloat = 0.0f;
  
  float val = EEPROM.get(addressEeprom[address] ,dataFloat);
  delay(100);
  // EEPROM.commit();
  // delay(100);
  return val;
}
/////////////////////////////////////////////////
////////////////
bool fn_eeRead(int address, bool valType) //bool
{
  bool val = EEPROM.read(addressEeprom[address]);
  delay(100);
  // EEPROM.commit();
  // delay(100);
  return val;
}
///////////////////////////////////
int fn_eeRead(int address, int valType) //int
{
  int dataInt = 00;

  int val = EEPROM.get(addressEeprom[address] , dataInt);
  delay(100);
  // EEPROM.commit();
  // delay(100);
  return val;
}
//==================================================================================================================
void fn_printToLcd(String LcdText, int lcdVal, byte column, byte line)
{ 
  lcd.setCursor(column,line);
  lcd.print(LcdText);
      
    if(lcdVal !=0)
    {
       lcd.print(lcdVal);
       if((lcdVal < 10 ) || (lcdVal < 100 && lcdVal > 90));
       {
        lcd.print(" ");
       }
    }
}
//////////////////
void fn_printToLcd(String LcdText, byte column, byte line)
{  
  lcd.setCursor(column,line);
  lcd.print(LcdText);
}
//====================================================================================================================
//////////////////////////////
void fn_feedOrWaiting(bool x)
{
  if(x)
  {
    digitalWrite(pinMosfet, 1); //feed
    Sprint("feed ");
    Sprintln(x);    
  }
  else
  {
    digitalWrite(pinMosfet, 0); //wait
    Sprint("feed ");
    Sprintln(x);
  }
}
///////////////////////////////
void fn_readDS18B20()
{
  DS18B20.requestTemperatures();
  nowTemp = DS18B20.getTempCByIndex(0);
  // nowTemp = nowTemp + (calTempROM);
  Sprintln("read ds18b20 ");
  Sprintln(nowTemp);
}
/////////////////////////////////////////////////////
///////////////////////////
void fn_feedEffect()
{
  flagEnRefreshLcd=1;
  int t=700;
  lcd.clear();

  for(byte i=0; i<16; i++)
  {
      fn_printToLcd("\xFF",0,i,1);
      delay(50);
  }
  lcd.clear();
  fn_printToLcd("  *** FEED ***  ",0,0);
  for(byte i=0; i<6; i++)
    {
      delay(t-=50);
      lcd.noDisplay();
      delay(t-=50);
      lcd.display();    
    }
  flagEnRefreshLcd=0;
}
/////////////////////
void fn_lcdRefresh()
{
  if(flagEnterExitMenu || flagEnRefreshLcd)
  return;

  static byte ai = 0;
  static bool isRightEffectLcd=0, isA=0;

  lcd.setCursor(0,0);
  lcd.print("T:");
  lcd.setCursor(2,0);
  lcd.print(nowTemp,2.1);
  lcd.setCursor(6,0);
  lcd.print(" ST:");
  lcd.setCursor(10,0);
  lcd.print(setTempROM,2.1);

  // Sprintln(F("ai"));
  // Sprintln(ai);

  if(!isRightEffectLcd)
  {
    fn_printToLcd("\xFF",0,ai,1);
    ai++;
    isRightEffectLcd = ai>5 ? 1 : 0;  
  }
  else
  {
    ai--;
    fn_printToLcd(" ",0,ai,1);
    isRightEffectLcd = ai<1 ? 0 : 1;
  }
  fn_printToLcd(isTempUpOrDown ?"u" : "d",timeWaiting-helpTimerWait-2,6,1);
  
  // fn_printToLcd("\x7E",timeWaiting,10,1);
  fn_printToLcd(isLcdPrSymbolForViewMode ? "*" : "#",helpByteAfnMind,14,1);
}
//////////////////////////
void fn_exitMenu()
{
  flagEnterExitMenu = 0;
  menuSel = 0;
  levelMenu = levelRefreshlcd;
  fn_readEepromVal();
  helpByteAfnMind = 9;
  fn_indexScreenLcd();
  countForExitMenu = -2;
}
/////////////////
void fn_subMenuWithFlag(String oneString, String twoString, int locMem)
{
	lcd.clear();
    fn_printToLcd(menuArr[menuSel],0,0);
    fn_printToLcd(subMenuArr[menuSel],0,1);

    fn_printToLcd(flagGeneral ? oneString : twoString,0,1);

    if(saveCheckByte>2) //double bEnter for save
    {
      fn_eeWrite(locMem,flagGeneral);
      saveCheckByte=0;
      isReadOnceA=0;
      fn_saveEffect();
    }
}
////////////////////////
void fn_lcdOnOffMode()
{
		// lcd.clear();    
		fn_printToLcd("--- OFF ---",3,0);
		lcd.setCursor(0,1);
		lcd.print("   T: ");
		lcd.print(nowTemp,2.1);
		lcd.print("C\xDF");
}